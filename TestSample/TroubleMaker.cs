﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace TestSample
{
    class TroubleMaker
    {
        //Comment by Geri for test

        private static Random rnd = new Random();
        

        //deszkákat tartalmazó tömbben készít hibákat
        //hibasz azt jelenti, hogy az algoritmus milyen eséllyel okozzon hibát a tömbben
        public static Deszka[] DeszkaErrorMaker(Deszka[] deszkak,double hibasz, int hibamod){
            
            Deszka[] Errordeszka=deszkak;

            //külön deszkánkként hiba generálása
            for (int i = 0; i < deszkak.Length; i++)
            {
                //hibaszázalékhoz random szám számítása
                double v = rnd.Next(0, 100);

                //ha nagyobb az érték mint a jó elemszázalék akkor hibát okoz
                if (v>=100-(hibasz)){

                    //színkódot range-en kívülire tolja
                    if(hibamod==1){
                        double v2 = rnd.Next(500, 1000);
                        double xv = rnd.Next(0, 100);
                        if (xv>=50)
                        {
                            v2 += double.Parse(Errordeszka[i].GetSzinkod());
                        }
                        else
                        {
                            v2 -= double.Parse(Errordeszka[i].GetSzinkod());
                        }

                        Errordeszka[i].SetGetSzinkod(v2.ToString());
                    }

                    //ítéletet algoritmus számára értelmetlen értékké váltja át
                    if (hibamod == 2)
                    {
                        Errordeszka[i].SetItelet("Nope");
                    }
                    

                }


            }
                //visszatérési érték a hibákat tartalmazó deszkákat tartalmazó tömb
                return Errordeszka;

        }








        //Fafajokat tartalmazó tömbben készít hibákat
        //hibasz azt jelenti, hogy az algoritmus milyen eséllyel okozzon hibát a tömbben
        public static Fafaj[] FafajErrorMaker(Fafaj[] fafajok, double hibasz, int hibamod)
        {

            
            Fafaj[] ErrorFafaj = fafajok;

            //külön fafajként hiba generálása
            for (int i = 0; i < fafajok.Length; i++)
            {
                //hibaszázalékhoz random szám számítása
                double v = rnd.Next(0, 100);
                //ha nagyobb az érték mint a jó elemszázalék akkor hibát okoz
                if (v >= (100 - hibasz))
                {

                    //a sötét limitben okoz out of range hibát
                    if (hibamod == 1)
                    {
                        int v2 = rnd.Next(-200, 0);
                        ErrorFafaj[i].SetSotet_limit(v2);
                    }

                    //a világos limitben okoz out of range hibát
                    if (hibamod == 2)
                    {
                        int v2 = rnd.Next(1200, 33333);
                        ErrorFafaj[i].SetVilagos_limit(v2);
                    }

                    //megcseréli a sötét és a világos limitet
                    if (hibamod == 3)
                    {
                        ErrorFafaj[i].SetVilagos_limit(ErrorFafaj[i].GetSotet_limit());
                        ErrorFafaj[i].SetSotet_limit(ErrorFafaj[i].GetVilagos_limit());
                    }


                }


            }

            //hibákat tatalmazó tömbböt adja vissza
            return ErrorFafaj;
        }




        //Termeléseket tartalmazó tömbben készít hibákat
        //hibasz azt jelenti, hogy az algoritmus milyen eséllyel okozzon hibát a tömbben
        public static Termeles[] TermelesErrorMaker(Termeles[] termelesek, double hibasz, int hibamod)
        {

            
            Termeles[] ErrorTermeles = termelesek;


            for (int i = 0; i < termelesek.Length; i++)
            {
                //hibaszázalékhoz random szám számítása
                double v = rnd.Next(0, 100);
                //ha nagyobb az érték mint a jó elemszázalék akkor hibát okoz
                if (v >= 100 - (hibasz))
                {

                    //értelmezhetetlen deszkaID-t állít be a termelésre
                    if (hibamod == 1)
                    {
                        int v2 = rnd.Next(-100, 0);
                        ErrorTermeles[i].SetDeszkaId(v2);
                    }

                    //értelmezhetetlen gépID-t állít be a termelésre
                    if (hibamod == 2)
                    {
                        int v2 = rnd.Next(-100, 0);
                        ErrorTermeles[i].SetGepId(v2);
                    }

                    //véletlen, nem túl mai értéket állít be dátumként termelésre
                    if (hibamod == 3)
                    {
                        int v2 = rnd.Next(1, 1000);
                        ErrorTermeles[i].SetDatum(new DateTime(v2, 1, 1, 0, 0, 0));
                    }


                }


            }

            //hibákat tartalmató termelésekkel feltöltött tömbböt ad vissza
            return ErrorTermeles;
        }








        //Szállítmányokat tartalmazó tömbben készít hibákat
        //hibasz azt jelenti, hogy az algoritmus milyen eséllyel okozzon hibát a tömbben
        public static Szallitmany[] SzallitmanyErrorMaker(Szallitmany[] szallitmanyok, double hibasz, int hibamod)
        {

            
            Szallitmany[] ErrorSzallitmany = szallitmanyok;


            for (int i = 0; i < szallitmanyok.Length; i++)
            {
                //hibaszázalékhoz random szám számítása
                double v = rnd.Next(0, 100);
                //ha nagyobb az érték mint a jó elemszázalék akkor hibát okoz
                if (v >= 100 - (hibasz))
                {

                    //értelmezhetetlen fafajID-t állít be a szállítmányhoz
                    if (hibamod == 1)
                    {
                        int v2 = rnd.Next(-100, 0);
                        ErrorSzallitmany[i].SetFafajId(v2);
                    }

                    //Beérkezési időpontra állít be értelmezhetetlen értéket
                    if (hibamod == 2)
                    {
                        int v2 = rnd.Next(-999, 0);
                        ErrorSzallitmany[i].SetIdopont(v2);
                    }

                    //véletlen, nem túl mai értéket állít be dátumként 
                    if (hibamod == 3)
                    {
                        int v2 = rnd.Next(1, 1000);
                        ErrorSzallitmany[i].SetDatum(new DateTime(v2, 1, 1, 0, 0, 0));
                    }


                }


            }


            return ErrorSzallitmany;
        }
    }
}
