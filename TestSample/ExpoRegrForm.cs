﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TestSample
{
    public partial class ExpoRegrForm : Form
    {
        ExpoRegr expr;
        public ExpoRegrForm()
        {
            InitializeComponent();
            
        }

        private void LinearRegresszio_Load(object sender, EventArgs e)
        {
       
        }

        private void button1_Click(object sender, EventArgs e)
        {
           int i = 1;
           foreach(Deszka d in Listak.deszkaLista)
           {
               int j = Int32.Parse(d.GetSzinkod());
               this.chart1.Series["Illesztés előtt"].Points.AddXY(i,j);
               i++;
          }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int xlength = Listak.deszkaLista.Count;
            double[] x = new double[xlength];
            for(int i=0;i<x.Length;i++)
            {
                x[i]=i+1;
            }
            double[] y = new double[xlength];
            int k=0;
            foreach(Deszka d in Listak.deszkaLista)
            {
                
                {
                    int j = Int32.Parse(d.GetSzinkod());
                    y[k] = j;
                    k++;
                }
            }
            
                expr = new ExpoRegr(x, y);
                for (int i = 1; i < x.Length; i++)
                {
                    this.chart1.Series["Illesztett"].Points.AddXY(i, expr.GetYValue(x[i]));
                }
            }

        private void button3_Click(object sender, EventArgs e)
        {
            if (expr != null)
            {
                MessageBox.Show("Az egyenelet: " + expr.GetEquation());
            }
            else
            {
                MessageBox.Show("Az egyenlet megtekintése előtt futtassa az illesztést!");
            }
        }
           
        }

        
    }

