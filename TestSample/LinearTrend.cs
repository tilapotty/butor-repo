﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSample
{
    class LinearTrend
    {
        private double B0;
        private double B1;

        public LinearTrend()
        {
            B0 = 0;
            B1 = 0;
        }

        // Béták kiszámítása normálegyenletek segítségével
        public void setBetas(double[] values)
        {
            // Értékek beillesztése az egyenletrendszer-mátrixba
            double[,] normalEquationMatrix = new double[2, 3];
            normalEquationMatrix[0, 0] = values.Length;
            normalEquationMatrix[0, 1] = getSumT(values.Length);
            normalEquationMatrix[0, 2] = getSumY(values);
            normalEquationMatrix[1, 0] = getSumT(values.Length);
            normalEquationMatrix[1, 1] = getSumTPow(values.Length);
            normalEquationMatrix[1, 2] = getSumTMultiplyY(values);

            // Normálegyenlet megoldása Gauss-eliminációval
            double[,] gaussMat = GaussianElimination.SolveEquationSystem(normalEquationMatrix); 

            B0 = gaussMat[0, 2];
            B1 = gaussMat[1, 2];
        }

        // Trendszámítás
        public double getTrend(double t)
        {
            return (B0 + (B1 * t));
        }

        private double getSumY(double[] y)
        {
            double sumY = 0;
            for (int t = 0; t < y.Length; t++)
            {
                sumY += y[t];
            }
            return sumY;
        }

        private double getSumTMultiplyY(double[] y)
        {
            double sumTMultiplyY = 0;
            for (int t = 0; t < y.Length; t++)
            {
                sumTMultiplyY += (t + 1) * y[t];
            }
            return sumTMultiplyY;
        }

        private double getSumT(int n)
        {
            return ((n * (n + 1)) / 2);
        }

        private double getSumTPow(int n)
        {
            return ((n * (n + 1) * ((2 * n) + 1)) / 6);
        }
    }
}
