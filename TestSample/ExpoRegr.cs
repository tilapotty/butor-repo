﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace TestSample
{
    class ExpoRegr
    {

        private double sumx = 0.0;
        private double sumy = 0.0;
        private double powervalues = 0;
        private double sumxsquares = 0.0;
        private double b = 0.0;
        private double a = 0.0;
        private double[] xvalues;
        private double[] yvalues;
        private int n;
       

        public ExpoRegr(double[] x, double[] y)
        {
            this.xvalues = x;
            this.yvalues = y;
            n = xvalues.Length;
            for (int i = 0; i < n; i++)
            {
                yvalues[i] = Math.Log(yvalues[i]);         
                sumx += xvalues[i];          
                sumy += yvalues[i];        
                sumxsquares += Math.Pow(xvalues[i], 2);         
                powervalues += xvalues[i] * yvalues[i];
            }
            
            a = (sumx * powervalues - sumxsquares * sumy) / (Math.Pow(sumx, 2) - n * sumxsquares);
            b = (sumy * sumx - powervalues * n) / (Math.Pow(sumx, 2) - n * sumxsquares);
            a = Math.Pow(Math.E, a);
         
           }

        public double GetYValue(double x)
        {
            return a * Math.Pow(Math.E, b * x);
        }

        public String GetEquation()
        {
            return a +"*e^("+b+"*x)";
        }

        public double[] GetAllXValues()
        {
            return this.yvalues;
        }

    }
}