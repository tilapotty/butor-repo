﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestSample
{

    public partial class ButorForm : Form
    {


        public ButorForm()
        {

            InitializeComponent();
        }


        private void jelszócsereToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PassChangerForm passChangerForm = new PassChangerForm();
            passChangerForm.Show();
        }

        private void limitváltoztatásToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LimitChangerForm limitChangerForm = new LimitChangerForm();
            limitChangerForm.Show();
        }

        private void mozgoAtlag_Click(object sender, EventArgs e)
        {
            // mozgóátlagform meghívása
            MozgoAtlagForm amozgoatlagform = new MozgoAtlagForm();
            amozgoatlagform.Show();

        }

        private void kissBAlg_Click(object sender, EventArgs e)
        {

            // dataGridView1.DataSource = TableCreatInDB();
            dataGridView1.Columns.Clear();
            dataGridView1.DataSource = SeasonalityAnalysis.tableWithSeasonality(TableCreatInDB());

            //DataGridView Tesztelése
            /*
              foreach (Deszka des in Listak.deszkaLista)
              {
                  dataGridView1.Columns[0].HeaderText = "ID";
                  dataGridView1.Columns[1].HeaderText = "Ítélet";
                  dataGridView1.Columns[2].HeaderText = "Színkód";
                  dataGridView1.Columns[3].HeaderText = "SzállítmányID";


                  String[] deszkaAdd = new String[] { String.Concat(des.GetId()), des.GetItelet(), des.GetSzinkod(), String.Concat(des.GetSzallitmanyId()) };
                  dataGridView1.Rows.Add(deszkaAdd);

              }
              */

        }
        public DataTable TableCreatInDB()
        {
            DataTable table = new DataTable();
            table.Columns.Add("year");
            table.Columns.Add("season");
            table.Columns.Add("value");

            foreach (Deszka desz in Listak.deszkaLista)
            {
                foreach (Szallitmany szalit in Listak.szallitLista)
                {
                    if (desz.GetSzallitmanyId() == szalit.GetId())
                    {

                        DataRow newRow = table.NewRow();
                        newRow["year"] = szalit.GetDatum();
                        newRow["season"] = szalit.GetDatum().Day;
                        newRow["value"] = desz.GetSzinkod();

                        table.Rows.Add(newRow);

                    }
                }
            }

            return table;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            ExpoRegrForm expr = new ExpoRegrForm();
            expr.Show();
        }

        private void ButorForm_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {//deszkalista az adatbázisból
            List<Deszka> deszkak = Listak.deszkaLista;

            //deszka array a listából
            Deszka[] deszkaA = deszkak.ToArray();


            //deszka array hibával tele rakva
            Deszka[] szinekA = TroubleMaker.DeszkaErrorMaker(deszkaA, 20, 1);

            //itt lesznek a szinek, a hibásak
            List<double> colors = new List<double> { };


            foreach (Deszka d in deszkaA)
            {
                colors.Add(Convert.ToDouble(d.GetSzinkod()));

            }
            KiugroErtek kiugro = new KiugroErtek();


            int i = 1;



            List<double> rosszColors = KiugrotSzamol(colors);

            foreach (int c in colors)
            {

                if (rosszColors.IndexOf(c) == -1)
                {
                    kiugro.chart1.Series["Szinek"].Points.AddXY(i, c);
                }
                else
                {
                    kiugro.chart1.Series["Kiugro"].Points.AddXY(i, c);
                }
                i++;
            }

            kiugro.Show();
        }

        public static List<double> KiugrotSzamol(List<double> list)
        {
            //medián meghatározása
            List<double> szamok = new List<double>(list) ;
            szamok.Sort();
            int db = szamok.Count();

            //medián kiszámítása
            double median;
            double q1 = 0;
            double q2 = 0;
            double a = db / 2;
            //ha a középső elem az jó indexnek
            if (a % 1 == 0)
            {
                median = szamok[(int)a];
            }
            //ha a páros a darabszám
            else
            {
                int index = (int)Math.Round(a);
                double sum = szamok[index] + szamok[index + 1];
                median = Math.Round(sum / 2);
            }
            //q1 beállítása
            double q1index = db * 0.25;
            if (q1index % 1 == 0)
            //ha a q1index egész
            {
                q1 = szamok[(int)q1index];
            }
            //ha q1index nem egész
            else
            {
                int index = (int)q1index;
                double sum = szamok[index] + szamok[index + 1];
                q1 = Math.Round(sum / 2);
            }
            //q2 beállítása
            double q2index = db * 0.75;
            if (q2index % 1 == 0)
            //ha a q2index egész
            {
                q2 = szamok[(int)q2index];
            }
            //ha q2index nem egész
            else
            {
                int index = (int)q2index;
                double sum = szamok[index] + szamok[index + 1];
                q2 = Math.Round(sum / 2);
            }
            double dif = q2 - q1;
            //Console.WriteLine("q1: " + q1);
            //Console.WriteLine("q2: " + q2);
            //Console.WriteLine("dif: " + dif);
            //Console.WriteLine("median: " + median);
            //foreach (int i in szamok)
            //{
            //    Console.WriteLine(i);
            //}
            //kigyűjtöm a rosszakat a nemjo Listbe
            List<double> nemjo = new List<double> { };
            double min = q1 - 1.5 * dif;
            double max = q2 + 1.5 * dif;
            foreach (int i in szamok)
            {
                if (i < min || i > max)
                {
                    nemjo.Add(i);
                    //Console.WriteLine("nemjó: " + i);

                }
            }
            return nemjo;
            //kiszedem az alapból a rosszakat
            //foreach (int i in nemjo) { szamok.Remove(i); }


        }

        private void button5_Click(object sender, EventArgs e)
        {
            TroubleMegjelenítő tr = new TroubleMegjelenítő();
            tr.Show();


        }




    }


}
