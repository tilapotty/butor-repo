﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestSample
{
    class Fafaj : TableManager<Fafaj>
    {
        //Az osztály adattagjai
        int id;
        String megnevezes;
        int vilagos_limit;
        int sotet_limit;
        //Üres konstruktor
        public Fafaj() { }

        //Másoló konstruktor
        public Fafaj(Fafaj f) 
        {
            this.id = f.GetId();
            this.megnevezes = f.GetMegnevezes();
            this.vilagos_limit = f.GetVilagos_limit();
            this.sotet_limit = f.GetSotet_limit();

        }

        //Az adatbázisból listába való töltés úgy, hogy egy példány az egy rekordot reprezentál
        public override List<Fafaj> ListaFeltolt()
        {
            List<Fafaj> lista = new List<Fafaj>();
            try
            {
                String path = @"..\..\TilaEroforrasok\SZTA2butor.s3db";
                SQLiteConnection dbConnection = new SQLiteConnection("Data Source=" + path);
                dbConnection.Open();
                string query = "SELECT ID, Megnevezes, Vilagos_limit, Sotet_limit FROM Fafaj";
                SQLiteCommand myCommand = new SQLiteCommand(query, dbConnection);
                SQLiteDataReader dreader = myCommand.ExecuteReader();
                while (dreader.Read())
                {
                    Fafaj fa = new Fafaj();
                    fa.id = dreader.GetInt32(dreader.GetOrdinal("ID"));
                    fa.megnevezes = dreader.GetString(dreader.GetOrdinal("Megnevezes"));
                    fa.vilagos_limit = dreader.GetInt32(dreader.GetOrdinal("Vilagos_limit"));
                    fa.sotet_limit = dreader.GetInt32(dreader.GetOrdinal("Sotet_limit"));
                    lista.Add(fa);

                }

                dbConnection.Close();
            }
            catch (Exception ex) { MessageBox.Show("Hiba " + ex.ToString()); }
            return lista;
        }

        public override void Update(Fafaj obj)
        {
            try
            {
                String path = @"..\..\TilaEroforrasok\SZTA2butor.s3db";
                SQLiteConnection dbConnection = new SQLiteConnection("Data Source=" + path);
                dbConnection.Open();
                string query = "UPDATE Fafaj set Vilagos_limit='" + obj.GetVilagos_limit() + "' ," + "Sotet_limit='" + obj.GetSotet_limit() + "' WHERE ID='" + obj.GetId() + "';";
                SQLiteCommand myCommand = new SQLiteCommand(query, dbConnection);
                myCommand.ExecuteNonQuery();
                dbConnection.Close();
            }
            catch (Exception ex) { MessageBox.Show("Hiba update: " + ex.ToString()); }
        }

        //Getterek
        public int GetId() { return this.id; }
        public int GetVilagos_limit() { return this.vilagos_limit; }
        public int GetSotet_limit() { return this.sotet_limit; }
        public String GetMegnevezes() { return this.megnevezes; }
        //Setterek
        public void SetId(int x) { this.id = x; }
        public void SetVilagos_limit(int x) { this.vilagos_limit = x; }
        public void SetSotet_limit(int x) { this.sotet_limit = x; }
        public void SetMegnevezes(String x) { this.megnevezes = x; }



    }
}
