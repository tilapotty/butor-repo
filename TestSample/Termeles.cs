﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestSample
{
    class Termeles : TableManager<Termeles>
    {
        //Az osztály adattagjai
        public int id;
        int deszkaId;
        public int gepId;
        public DateTime datum;

        //Üres konstruktor
        public Termeles() { }
        //Másoló konstruktor
        public Termeles(Termeles t)
        {
            this.id = t.GetId();
            this.deszkaId = t.GetDeszkaId();
            this.gepId = t.GetGepId();
            this.datum = t.GetDatum();

        }
        public override List<Termeles> ListaFeltolt()
        {
            List<Termeles> lista = new List<Termeles>();

            try
            {
                String path = @"..\..\TilaEroforrasok\SZTA2butor.s3db";
                SQLiteConnection dbConnection = new SQLiteConnection("Data Source=" + path);
                dbConnection.Open();
                string query = "SELECT ID, DeszkaID, GepID, Datum FROM Termeles";
                SQLiteCommand myCommand = new SQLiteCommand(query, dbConnection);
                SQLiteDataReader dreader = myCommand.ExecuteReader();

                while (dreader.Read())
                {
                    Termeles term = new Termeles();
                    term.id = dreader.GetInt32(dreader.GetOrdinal("ID"));
                    term.deszkaId = dreader.GetInt32(dreader.GetOrdinal("DeszkaID"));
                    term.gepId = dreader.GetInt32(dreader.GetOrdinal("GepID"));
                    term.datum = dreader.GetDateTime(dreader.GetOrdinal("Datum"));
                    lista.Add(term);
                }

                dbConnection.Close();
            }

            catch (Exception ex) { MessageBox.Show("Hiba " + ex.ToString()); }

            return lista;
        }

        public override void Update(Termeles obj)
        {
            throw new NotImplementedException();
        }

        //Getterek
        public int GetId() { return this.id; }
        public int GetDeszkaId() { return this.deszkaId; }
        public int GetGepId() { return this.gepId; }
        public DateTime GetDatum() { return this.datum; }

        //Setterek
        public void SetId(int x) { this.id = x; }
        public void SetDeszkaId(int x) { this.deszkaId = x; }
        public void SetGepId(int x) { this.gepId = x; }
        public void SetDatum(DateTime x) { this.datum = x; }
    }
}
