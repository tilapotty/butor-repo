﻿namespace TestSample
{
    partial class LimitChangerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonJelszo = new System.Windows.Forms.Button();
            this.textBoxJelszo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonMent = new System.Windows.Forms.Button();
            this.textBoxHatar2 = new System.Windows.Forms.TextBox();
            this.textBoxHatar1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxFafaj = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonJelszo);
            this.groupBox1.Controls.Add(this.textBoxJelszo);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(215, 76);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "A módosításhoz jelszó szükséges";
            // 
            // buttonJelszo
            // 
            this.buttonJelszo.Location = new System.Drawing.Point(142, 44);
            this.buttonJelszo.Name = "buttonJelszo";
            this.buttonJelszo.Size = new System.Drawing.Size(63, 20);
            this.buttonJelszo.TabIndex = 2;
            this.buttonJelszo.Text = "OK";
            this.buttonJelszo.UseVisualStyleBackColor = true;
            this.buttonJelszo.Click += new System.EventHandler(this.buttonJelszo_Click);
            // 
            // textBoxJelszo
            // 
            this.textBoxJelszo.Location = new System.Drawing.Point(9, 44);
            this.textBoxJelszo.Name = "textBoxJelszo";
            this.textBoxJelszo.Size = new System.Drawing.Size(127, 20);
            this.textBoxJelszo.TabIndex = 1;
            this.textBoxJelszo.UseSystemPasswordChar = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Jelszó:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonMent);
            this.groupBox2.Controls.Add(this.textBoxHatar2);
            this.groupBox2.Controls.Add(this.textBoxHatar1);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.comboBoxFafaj);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(15, 102);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(212, 177);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Beállítások";
            // 
            // buttonMent
            // 
            this.buttonMent.Location = new System.Drawing.Point(78, 143);
            this.buttonMent.Name = "buttonMent";
            this.buttonMent.Size = new System.Drawing.Size(85, 28);
            this.buttonMent.TabIndex = 6;
            this.buttonMent.Text = "Mentés";
            this.buttonMent.UseVisualStyleBackColor = true;
            this.buttonMent.Click += new System.EventHandler(this.buttonMent_Click);
            // 
            // textBoxHatar2
            // 
            this.textBoxHatar2.Location = new System.Drawing.Point(78, 80);
            this.textBoxHatar2.Name = "textBoxHatar2";
            this.textBoxHatar2.Size = new System.Drawing.Size(107, 20);
            this.textBoxHatar2.TabIndex = 5;
            // 
            // textBoxHatar1
            // 
            this.textBoxHatar1.Location = new System.Drawing.Point(78, 54);
            this.textBoxHatar1.Name = "textBoxHatar1";
            this.textBoxHatar1.Size = new System.Drawing.Size(107, 20);
            this.textBoxHatar1.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Határ 2:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Határ 1:";
            // 
            // comboBoxFafaj
            // 
            this.comboBoxFafaj.FormattingEnabled = true;
            this.comboBoxFafaj.Location = new System.Drawing.Point(78, 27);
            this.comboBoxFafaj.Name = "comboBoxFafaj";
            this.comboBoxFafaj.Size = new System.Drawing.Size(107, 21);
            this.comboBoxFafaj.TabIndex = 1;
            this.comboBoxFafaj.SelectedIndexChanged += new System.EventHandler(this.comboBoxFafaj_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Fafaj:";
            // 
            // LimitChangerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(242, 293);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "LimitChangerForm";
            this.Text = "Limit változtatás";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonJelszo;
        private System.Windows.Forms.TextBox textBoxJelszo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxFafaj;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonMent;
        private System.Windows.Forms.TextBox textBoxHatar2;
        private System.Windows.Forms.TextBox textBoxHatar1;
    }
}