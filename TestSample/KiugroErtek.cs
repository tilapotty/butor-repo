﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestSample
{
    public partial class KiugroErtek : Form
    {
        public KiugroErtek()
        {
            InitializeComponent();
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (var series in this.chart1.Series)
            {
                series.Points.Clear();
            }
            List<double> colors = new List<double> { 
                1, 2, 3, 7, 5, -10, 7, 8, 9, 10, 22, 12 };
            List<double> JoColors =ButorForm.KiugrotSzamol(colors);
            KiugroErtek kiugro = new KiugroErtek();
            int i = 1;
            foreach (int c in colors)
            {

                if (JoColors.IndexOf(c) == -1)
                {
                    this.chart1.Series["Szinek"].Points.AddXY(i, c);
                }
                else
                {
                    this.chart1.Series["Kiugro"].Points.AddXY(i, c);
                }
                i++;
            }
            
        }

        private void chart2_Click(object sender, EventArgs e)
        {

        }

    }
}
