﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSample
{
    abstract class TableManager<T>
    {
        public abstract List<T> ListaFeltolt();

        public abstract void Update(T obj);
    }
}
