﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestSample
{
    class Szallitmany : TableManager<Szallitmany>
    {
        //Az osztály adattagjai
        int id;
        int faFajId;
        DateTime datum;
        int idoPont;
        int BeSzallito_azonosito;

        //Üres konstruktor
        public Szallitmany() { }

        //Másoló konstruktor
        public Szallitmany(Szallitmany sz) 
        {
            this.id = sz.GetId();
            this.faFajId = sz.GetFafajId();
            this.datum = sz.GetDatum();
            this.idoPont = sz.GetIdopont();
            this.BeSzallito_azonosito = sz.GetBeSzallito_azonosito();
        }
        //Az adatbázisból listába való töltés úgy, hogy egy példány az egy rekordot reprezentál
        public override List<Szallitmany> ListaFeltolt()
        {
            List<Szallitmany> lista = new List<Szallitmany>();
            try
            {
                String path = @"..\..\TilaEroforrasok\SZTA2butor.s3db";
                SQLiteConnection dbConnection = new SQLiteConnection("Data Source=" + path);
                dbConnection.Open();
                string query = "SELECT ID, FafajID, Datum, Idopont, Beszallito_azonosito FROM Szallitmany";
                SQLiteCommand myCommand = new SQLiteCommand(query, dbConnection);
                SQLiteDataReader dreader = myCommand.ExecuteReader();

                while (dreader.Read())
                {
                    Szallitmany szallit = new Szallitmany();
                    szallit.id = dreader.GetInt32(dreader.GetOrdinal("ID"));
                    szallit.faFajId = dreader.GetInt32(dreader.GetOrdinal("FafajID"));
                    szallit.datum = dreader.GetDateTime(dreader.GetOrdinal("Datum"));
                    szallit.idoPont = dreader.GetInt32(dreader.GetOrdinal("Idopont"));
                    szallit.BeSzallito_azonosito = dreader.GetInt32(dreader.GetOrdinal("Beszallito_azonosito"));
                    lista.Add(szallit);
                }
                dbConnection.Close();
            }
            catch (Exception ex) { MessageBox.Show("Hiba " + ex.ToString()); }

            return lista;
        }

        public override void Update(Szallitmany obj)
        {
            throw new NotImplementedException();
        }

        //Getterek
        public int GetId() { return this.id; }
        public int GetFafajId() { return this.faFajId; }
        public DateTime GetDatum() { return this.datum; }
        public int GetIdopont() { return this.idoPont; }
        public int GetBeSzallito_azonosito() { return this.BeSzallito_azonosito; }

        //Setterek
        public void SetId(int x) { this.id = x; }
        public void SetFafajId(int x) { this.faFajId = x; }
        public void SetDatum(DateTime x) { this.datum = x; }
        public void SetIdopont(int x) { this.idoPont = x; }
        public void SetBeSzallito_azonosito(int x) { this.BeSzallito_azonosito = x; }

    }
}
