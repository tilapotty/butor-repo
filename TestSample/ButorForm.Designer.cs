﻿namespace TestSample
{
    partial class ButorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.fafaj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.szinkod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.muszak = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fájlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.onlineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.offlineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.beállításokToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jelszócsereToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.limitváltoztatásToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aProgramrólToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.névjegyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kissBAlg = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(158, 304);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(140, 30);
            this.button1.TabIndex = 0;
            this.button1.Text = "Mozgóátlag";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.mozgoAtlag_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(306, 304);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(140, 30);
            this.button2.TabIndex = 1;
            this.button2.Text = "Kiugró értékek";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(602, 304);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(140, 30);
            this.button4.TabIndex = 3;
            this.button4.Text = "Exponenciális Regresszió";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fafaj,
            this.szinkod,
            this.ido,
            this.muszak});
            this.dataGridView1.Location = new System.Drawing.Point(12, 27);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(730, 269);
            this.dataGridView1.TabIndex = 4;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // fafaj
            // 
            this.fafaj.HeaderText = "Fafaj";
            this.fafaj.Name = "fafaj";
            // 
            // szinkod
            // 
            this.szinkod.HeaderText = "Színkód";
            this.szinkod.Name = "szinkod";
            // 
            // ido
            // 
            this.ido.HeaderText = "Idő";
            this.ido.Name = "ido";
            // 
            // muszak
            // 
            this.muszak.HeaderText = "Műszak";
            this.muszak.Name = "muszak";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fájlToolStripMenuItem,
            this.beállításokToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(775, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fájlToolStripMenuItem
            // 
            this.fájlToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.onlineToolStripMenuItem,
            this.offlineToolStripMenuItem});
            this.fájlToolStripMenuItem.Name = "fájlToolStripMenuItem";
            this.fájlToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fájlToolStripMenuItem.Text = "Fájl";
            // 
            // onlineToolStripMenuItem
            // 
            this.onlineToolStripMenuItem.Name = "onlineToolStripMenuItem";
            this.onlineToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.onlineToolStripMenuItem.Text = "Online";
            // 
            // offlineToolStripMenuItem
            // 
            this.offlineToolStripMenuItem.Name = "offlineToolStripMenuItem";
            this.offlineToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.offlineToolStripMenuItem.Text = "Offline";
            // 
            // beállításokToolStripMenuItem
            // 
            this.beállításokToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.jelszócsereToolStripMenuItem,
            this.limitváltoztatásToolStripMenuItem});
            this.beállításokToolStripMenuItem.Name = "beállításokToolStripMenuItem";
            this.beállításokToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.beállításokToolStripMenuItem.Text = "Műveletek";
            // 
            // jelszócsereToolStripMenuItem
            // 
            this.jelszócsereToolStripMenuItem.Name = "jelszócsereToolStripMenuItem";
            this.jelszócsereToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.jelszócsereToolStripMenuItem.Text = "Jelszócsere";
            this.jelszócsereToolStripMenuItem.Click += new System.EventHandler(this.jelszócsereToolStripMenuItem_Click);
            // 
            // limitváltoztatásToolStripMenuItem
            // 
            this.limitváltoztatásToolStripMenuItem.Name = "limitváltoztatásToolStripMenuItem";
            this.limitváltoztatásToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.limitváltoztatásToolStripMenuItem.Text = "Limitváltoztatás";
            this.limitváltoztatásToolStripMenuItem.Click += new System.EventHandler(this.limitváltoztatásToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aProgramrólToolStripMenuItem,
            this.névjegyToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.aboutToolStripMenuItem.Text = "Súgó";
            // 
            // aProgramrólToolStripMenuItem
            // 
            this.aProgramrólToolStripMenuItem.Name = "aProgramrólToolStripMenuItem";
            this.aProgramrólToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.aProgramrólToolStripMenuItem.Text = "Súgó";
            // 
            // névjegyToolStripMenuItem
            // 
            this.névjegyToolStripMenuItem.Name = "névjegyToolStripMenuItem";
            this.névjegyToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.névjegyToolStripMenuItem.Text = "Névjegy";
            // 
            // kissBAlg
            // 
            this.kissBAlg.Location = new System.Drawing.Point(12, 304);
            this.kissBAlg.Name = "kissBAlg";
            this.kissBAlg.Size = new System.Drawing.Size(140, 30);
            this.kissBAlg.TabIndex = 6;
            this.kissBAlg.Text = "Kiss B. Algoritmus";
            this.kissBAlg.UseVisualStyleBackColor = true;
            this.kissBAlg.Click += new System.EventHandler(this.kissBAlg_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(456, 304);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(140, 30);
            this.button5.TabIndex = 7;
            this.button5.Text = "Error Készítés";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // ButorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(775, 371);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.kissBAlg);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ButorForm";
            this.Text = "Bútor";
            this.Load += new System.EventHandler(this.ButorForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn fafaj;
        private System.Windows.Forms.DataGridViewTextBoxColumn szallitmany;
        private System.Windows.Forms.DataGridViewTextBoxColumn szinkod;
        private System.Windows.Forms.DataGridViewTextBoxColumn ido;
        private System.Windows.Forms.DataGridViewTextBoxColumn muszak;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fájlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem onlineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem offlineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem beállításokToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aProgramrólToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem névjegyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jelszócsereToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem limitváltoztatásToolStripMenuItem;
        private System.Windows.Forms.Button kissBAlg;
        private System.Windows.Forms.Button button5;
    }
}