﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestSample
{
    class Felhasznalo : TableManager<Felhasznalo>
    {
        //Az osztály adattagjai
        String nev;
        String jelszo;
        //Üres konstruktor
        public Felhasznalo() { }

        //Getterek
        public String GetNev() { return this.nev; }
        public String GetJelszo() { return this.jelszo; }

        //Setterek
        public void SetNev(String x) { this.nev = x; }
        public void SetJelszo(String x) { this.jelszo = x; }

        public override List<Felhasznalo> ListaFeltolt()
        {
            List<Felhasznalo> lista = new List<Felhasznalo>();

            try
            {
                String path = @"..\..\TilaEroforrasok\SZTA2butor.s3db";
                SQLiteConnection dbConnection = new SQLiteConnection("Data Source=" + path);
                dbConnection.Open();
                string query = "SELECT Nev, Jelszo FROM Felhasznalo";
                SQLiteCommand myCommand = new SQLiteCommand(query, dbConnection);
                SQLiteDataReader dreader = myCommand.ExecuteReader();

                while (dreader.Read())
                {
                    Felhasznalo uj = new Felhasznalo();
                    uj.nev = dreader.GetString(dreader.GetOrdinal("Nev"));
                    uj.jelszo = dreader.GetString(dreader.GetOrdinal("Jelszo"));
                    lista.Add(uj);
                }

                dbConnection.Close();
            }

            catch (Exception ex) { MessageBox.Show("Hiba " + ex.ToString()); }


            return lista;

        }

        public override void Update(Felhasznalo obj)
        {
            try
            {
                String path = @"..\..\TilaEroforrasok\SZTA2butor.s3db";
                SQLiteConnection dbConnection = new SQLiteConnection("Data Source=" + path);
                dbConnection.Open();
                string query = "UPDATE Felhasznalo set Jelszo='" + obj.GetJelszo() + "' WHERE Nev='" + obj.GetNev() + "';";
                SQLiteCommand myCommand = new SQLiteCommand(query, dbConnection);
                myCommand.ExecuteNonQuery();
                dbConnection.Close();
            }
            catch (Exception ex) { MessageBox.Show("Hiba update: " + ex.ToString()); }
        }
    }
}
