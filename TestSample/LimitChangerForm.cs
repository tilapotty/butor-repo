﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestSample
{
    public partial class LimitChangerForm : Form
    {
        bool bejel;


        public LimitChangerForm()
        {
            bejel = false;
            InitializeComponent();
            comboBoxFafaj.Text = "Fafaj kiválasztása";
        }

        private void buttonJelszo_Click(object sender, EventArgs e)
        {
            DataTable dtblDataSource = new DataTable();

            if (Bejelentkezes() == true)
            {
                foreach (Fafaj fa in Listak.faLista)
                {
                    comboBoxFafaj.Items.Add(fa.GetMegnevezes());

                }
            }
        }

        public bool Bejelentkezes()
        {

            foreach (Felhasznalo felh in Listak.felhLista)
            {
                if (textBoxJelszo.Text == felh.GetJelszo())
                {
                    MessageBox.Show("Sikeres Bejelntkezés");
                    bejel = true;
                    return true;
                }


            }
            MessageBox.Show("Hibás jelszó");
            return false;
        }

        private void comboBoxFafaj_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < Listak.faLista.Count; i++)
            {
                if (comboBoxFafaj.SelectedItem == Listak.faLista[i].GetMegnevezes())
                {
                    textBoxHatar1.Text = "" + Listak.faLista[i].GetSotet_limit();
                    textBoxHatar2.Text = "" + Listak.faLista[i].GetVilagos_limit();

                }
            }
        }

        private void buttonMent_Click(object sender, EventArgs e)
        {
            if (!bejel)
            {
                MessageBox.Show("A mentéshez bejelentkezés szükséges !");
                return;
            }

            if (!TextBoxElleneorzes())
            {
                MessageBox.Show("Csak számot lehet megadni !");
                return;
            }

            if (MessageBox.Show("A változások érintik az adatbázis. Folytatja a műveletet ?",
                "Mentés", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                FafajUpdate();
            }
        }

        public void FafajUpdate()
        {
            Fafaj updateFa = new Fafaj();
            foreach (Fafaj fa in Listak.faLista)
            {
                if (comboBoxFafaj.SelectedItem == fa.GetMegnevezes())
                {
                    updateFa.SetId(fa.GetId());
                    updateFa.SetSotet_limit(int.Parse(textBoxHatar1.Text));
                    updateFa.SetVilagos_limit(int.Parse(textBoxHatar2.Text));

                    fa.SetSotet_limit(int.Parse(textBoxHatar1.Text));
                    fa.SetVilagos_limit(int.Parse(textBoxHatar2.Text));
                    fa.Update(updateFa);
                    MessageBox.Show("Sikeres Adatbázis módosítás !");
                    return;
                }

            }
        }

        public bool TextBoxElleneorzes()
        {
            try
            {
                int szam = int.Parse(textBoxHatar2.Text);
                int szam1 = int.Parse(textBoxHatar1.Text);
            }
            catch (Exception nne)
            {
                // MessageBox.Show("szam " + nne.ToString());
                return false;
            }

            return true;
        }

    }
}
