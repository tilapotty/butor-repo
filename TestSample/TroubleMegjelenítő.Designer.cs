﻿namespace TestSample
{
    partial class TroubleMegjelenítő
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TroubleList = new System.Windows.Forms.ListBox();
            this.Typebx = new System.Windows.Forms.ListBox();
            this.NotroubleGrid = new System.Windows.Forms.DataGridView();
            this.TroubleGrid = new System.Windows.Forms.DataGridView();
            this.HibaPercent = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ment_gomb = new System.Windows.Forms.Button();
            this.login_gomb = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.pswBox = new System.Windows.Forms.TextBox();
            this.logg = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.NotroubleGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TroubleGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // TroubleList
            // 
            this.TroubleList.FormattingEnabled = true;
            this.TroubleList.Items.AddRange(new object[] {
            "Deszka",
            "Fafaj",
            "Szállítmány",
            "Termelés"});
            this.TroubleList.Location = new System.Drawing.Point(9, 288);
            this.TroubleList.Margin = new System.Windows.Forms.Padding(2);
            this.TroubleList.Name = "TroubleList";
            this.TroubleList.Size = new System.Drawing.Size(91, 69);
            this.TroubleList.TabIndex = 1;
            this.TroubleList.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // Typebx
            // 
            this.Typebx.FormattingEnabled = true;
            this.Typebx.Location = new System.Drawing.Point(287, 288);
            this.Typebx.Margin = new System.Windows.Forms.Padding(2);
            this.Typebx.Name = "Typebx";
            this.Typebx.Size = new System.Drawing.Size(91, 69);
            this.Typebx.TabIndex = 2;
            this.Typebx.SelectedIndexChanged += new System.EventHandler(this.listBox2_SelectedIndexChanged);
            // 
            // NotroubleGrid
            // 
            this.NotroubleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.NotroubleGrid.Location = new System.Drawing.Point(0, 0);
            this.NotroubleGrid.Margin = new System.Windows.Forms.Padding(2);
            this.NotroubleGrid.Name = "NotroubleGrid";
            this.NotroubleGrid.RowTemplate.Height = 24;
            this.NotroubleGrid.Size = new System.Drawing.Size(377, 283);
            this.NotroubleGrid.TabIndex = 4;
            // 
            // TroubleGrid
            // 
            this.TroubleGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TroubleGrid.Location = new System.Drawing.Point(396, 0);
            this.TroubleGrid.Margin = new System.Windows.Forms.Padding(2);
            this.TroubleGrid.Name = "TroubleGrid";
            this.TroubleGrid.RowTemplate.Height = 24;
            this.TroubleGrid.Size = new System.Drawing.Size(400, 283);
            this.TroubleGrid.TabIndex = 5;
            // 
            // HibaPercent
            // 
            this.HibaPercent.Location = new System.Drawing.Point(205, 314);
            this.HibaPercent.Margin = new System.Windows.Forms.Padding(2);
            this.HibaPercent.Name = "HibaPercent";
            this.HibaPercent.Size = new System.Drawing.Size(76, 20);
            this.HibaPercent.TabIndex = 6;
            this.HibaPercent.Text = "10";
            this.HibaPercent.TextChanged += new System.EventHandler(this.HibaPercent_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(104, 314);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Hibaszázalék(%):";
            // 
            // ment_gomb
            // 
            this.ment_gomb.Location = new System.Drawing.Point(493, 366);
            this.ment_gomb.Name = "ment_gomb";
            this.ment_gomb.Size = new System.Drawing.Size(105, 23);
            this.ment_gomb.TabIndex = 8;
            this.ment_gomb.Text = "Mentés";
            this.ment_gomb.UseVisualStyleBackColor = true;
            this.ment_gomb.Click += new System.EventHandler(this.ment_gomb_Click);
            // 
            // login_gomb
            // 
            this.login_gomb.Location = new System.Drawing.Point(552, 302);
            this.login_gomb.Name = "login_gomb";
            this.login_gomb.Size = new System.Drawing.Size(108, 23);
            this.login_gomb.TabIndex = 9;
            this.login_gomb.Text = "Bejelentkezés";
            this.login_gomb.UseVisualStyleBackColor = true;
            this.login_gomb.Click += new System.EventHandler(this.login_gomb_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(407, 304);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Jelszó:";
            // 
            // pswBox
            // 
            this.pswBox.Location = new System.Drawing.Point(452, 302);
            this.pswBox.Name = "pswBox";
            this.pswBox.Size = new System.Drawing.Size(84, 20);
            this.pswBox.TabIndex = 11;
            this.pswBox.UseSystemPasswordChar = true;
            // 
            // logg
            // 
            this.logg.AutoSize = true;
            this.logg.Location = new System.Drawing.Point(682, 307);
            this.logg.Name = "logg";
            this.logg.Size = new System.Drawing.Size(78, 13);
            this.logg.TabIndex = 12;
            this.logg.Text = "Jelentkezz be !";
            // 
            // TroubleMegjelenítő
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 401);
            this.Controls.Add(this.logg);
            this.Controls.Add(this.pswBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.login_gomb);
            this.Controls.Add(this.ment_gomb);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.HibaPercent);
            this.Controls.Add(this.TroubleGrid);
            this.Controls.Add(this.NotroubleGrid);
            this.Controls.Add(this.Typebx);
            this.Controls.Add(this.TroubleList);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "TroubleMegjelenítő";
            this.Text = "TroubleMaker Viewer";
            this.Load += new System.EventHandler(this.TroubleMegjelenítő_Load);
            ((System.ComponentModel.ISupportInitialize)(this.NotroubleGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TroubleGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox TroubleList;
        private System.Windows.Forms.ListBox Typebx;
        private System.Windows.Forms.DataGridView NotroubleGrid;
        private System.Windows.Forms.DataGridView TroubleGrid;
        private System.Windows.Forms.TextBox HibaPercent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ment_gomb;
        private System.Windows.Forms.Button login_gomb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox pswBox;
        private System.Windows.Forms.Label logg;

    }
}