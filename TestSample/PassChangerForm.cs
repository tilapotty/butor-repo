﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestSample
{
    public partial class PassChangerForm : Form
    {
        public PassChangerForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MezokEllenorzese() == true)
            {
                if (MessageBox.Show("A változások érintik az adatbázis. Folytatja a műveletet ?",
                    "Mentés", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    JelszoValtas();
                }

            }


        }

        public void JelszoValtas()
        {
            Felhasznalo updateFelh = new Felhasznalo();
            updateFelh.SetNev("admin");
            updateFelh.SetJelszo(textBoxUj2.Text);

            foreach (Felhasznalo felh in Listak.felhLista)
            {
                if (felh.GetNev().Equals(updateFelh.GetNev()))
                {
                    felh.SetJelszo(updateFelh.GetJelszo());
                    felh.Update(updateFelh);
                    MessageBox.Show("Sikeres jelszó módosítás");
                    return;
                }
            }

        }

        public bool MezokEllenorzese()
        {
            Char[] szam = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            bool szamOK = false;

            foreach (Felhasznalo felh in Listak.felhLista)
            {
                if (textBoxRegi.Text != felh.GetJelszo())
                {
                    MessageBox.Show("Nincs ilyen (régi)jelszó az adatbázisba !");
                    return false;
                }
            }

            if (textBoxUj1.Text == "" || textBoxUj2.Text == "")
            {
                MessageBox.Show("A mezők kitöltése kötelező !");
                return false;
            }

            if (textBoxUj2.Text.Length < 3)
            {
                MessageBox.Show("Az új jelszónak legalább 3 karakterűnek kell lennie !");
                return false;
            }

            if (textBoxUj1.Text != textBoxUj2.Text)
            {
                MessageBox.Show("Nem egyeznek meg az 'új jelszó' mezők !");
                return false;
            }

            for (int i = 0; i < szam.Length; i++)
            {
                if (textBoxUj2.Text.ToCharArray().Contains(szam[i])) { szamOK = true; }

            }

            if (!szamOK)
            {
                MessageBox.Show("Az új jelszónak tartalmaznia kell számot !");
                return false;
            }

            return true;
        }
    }
}
