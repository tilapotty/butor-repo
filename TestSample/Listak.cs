﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSample
{
    class Listak
    {
        //Az táblákat reprezentáló osztályok példányai
        static Fafaj faf = new Fafaj();
        static Deszka deszka = new Deszka();
        static Szallitmany szallitmany = new Szallitmany();
        static Termeles termel = new Termeles();
        static Felhasznalo felh = new Felhasznalo();

        //Listák inicializálása
        public static List<Fafaj> faLista = faf.ListaFeltolt();
        public static List<Deszka> deszkaLista = deszka.ListaFeltolt();
        public static List<Szallitmany> szallitLista = szallitmany.ListaFeltolt();
        public static List<Termeles> termelLista = termel.ListaFeltolt();
        public static List<Felhasznalo> felhLista = felh.ListaFeltolt();


    }
}
