﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestSample
{
    class Deszka : TableManager<Deszka>
    {
        //Az osztály adattagjai
        int id;
        //DB-ben való tárolás miatt ilyen a típusa
        String szinkod;
        String itelet;
        int szallitmanyId;

        //üres konstruktor
        public Deszka() { }

        //Másoló konstruktor
        public Deszka(Deszka d) 
        {
            this.id = d.GetId();
            this.szinkod = d.GetSzinkod();
            this.itelet = d.GetItelet();
            this.szallitmanyId = d.GetSzallitmanyId();
        }

        //Az adatbázisból listába való töltés úgy, hogy egy példány az egy rekordot reprezentál
        public override List<Deszka> ListaFeltolt()
        {
            List<Deszka> lista = new List<Deszka>();

            try
            {
                String path = @"..\..\TilaEroforrasok\SZTA2butor.s3db";
                SQLiteConnection dbConnection = new SQLiteConnection("Data Source=" + path);
                dbConnection.Open();
                string query = "SELECT ID, Szinkod, Itelet, SzallitmanyID From Deszka";
                SQLiteCommand myCommand = new SQLiteCommand(query, dbConnection);
                SQLiteDataReader dreader = myCommand.ExecuteReader();

                while (dreader.Read())
                {
                    Deszka deszka = new Deszka();
                    deszka.id = dreader.GetInt32(dreader.GetOrdinal("ID"));
                    deszka.szinkod = dreader.GetString(dreader.GetOrdinal("Szinkod"));
                    deszka.itelet = dreader.GetString(dreader.GetOrdinal("Itelet"));
                    deszka.szallitmanyId = dreader.GetInt32(dreader.GetOrdinal("SzallitmanyID"));
                    lista.Add(deszka);
                }

                dbConnection.Close();
            }
            catch (Exception ex) { MessageBox.Show("Hiba " + ex.ToString()); }

            return lista;
        }

        public override void Update(Deszka obj)
        {
            try
            {
                String path = @"..\..\TilaEroforrasok\SZTA2butor.s3db";
                SQLiteConnection dbConnection = new SQLiteConnection("Data Source=" + path);
                dbConnection.Open();
                string query = "UPDATE Deszka set Szinkod='" + obj.GetSzinkod() + "' ," + "Itelet='" + obj.GetItelet() + "' WHERE ID='" + obj.GetId() + "';";
                SQLiteCommand myCommand = new SQLiteCommand(query, dbConnection);
                myCommand.ExecuteNonQuery();
                dbConnection.Close();
            }
            catch (Exception ex) { MessageBox.Show("Hiba update: " + ex.ToString()); }
        }

        //Getterek
        public int GetId() { return this.id; }
        public String GetSzinkod() { return this.szinkod; }
        public String GetItelet() { return this.itelet; }
        public int GetSzallitmanyId() { return this.szallitmanyId; }

        //Setterek
        public void SetId(int x) { this.id = x; }
        public void SetGetSzinkod(String x) { this.szinkod = x; }
        public void SetItelet(String x) { this.itelet = x; }
        public void SetSzallitmanyId(int x) { this.szallitmanyId = x; }
    }
}
