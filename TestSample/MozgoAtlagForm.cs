﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace TestSample
{
    public partial class MozgoAtlagForm : Form
    {

       
        public MozgoAtlagForm()
        {
            InitializeComponent();          
        }

        private void Form2_Load(object sender, EventArgs e)
        {

            //listákból a szinkódok kinyerése és double tömbbe rakása
            double[] szinKodok = new double[Listak.deszkaLista.Count];
            int x = 0;
            foreach (Deszka desz in Listak.deszkaLista)
            {
                szinKodok[x] = Double.Parse(desz.GetSzinkod());
                x++;
            }

            //mozgóátlag függvény meghívása
            double[] mozgoAtlagoltSzinkodok = MozgoAtlag.mozgoAtlag(3, szinKodok);



            //ábrázolás régi és új adatok
            for (int i = 0; i < mozgoAtlagoltSzinkodok.Length-2; i++)
            {
                
                chart2.Series[0].Points.Add(szinKodok[i]);
                chart2.Series[1].Points.Add(mozgoAtlagoltSzinkodok[i+2]);

                //string[] row = new string[] { szinKodok[i].ToString(), mozgoAtlagoltSzinkodok[i].ToString() };
            };

            
        }

        private void chart2_Click(object sender, EventArgs e)
        {

        }

      
       

    
      
    }
}
