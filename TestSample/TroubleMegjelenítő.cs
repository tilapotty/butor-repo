﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestSample
{
    public partial class TroubleMegjelenítő : Form
    {

        Deszka[] deszkaA;
        Fafaj[] faA;
        Termeles[] termelA;
        Szallitmany[] szallA;

        Deszka[] deszkaB;
        Fafaj[] faB;
        Termeles[] termelB;
        Szallitmany[] szallB;


        public TroubleMegjelenítő()
        {
            InitializeComponent();
            List<Deszka> tesztkak = Listak.deszkaLista;
            List<Fafaj> tesztfa = Listak.faLista;
            List<Termeles> tesztterm = Listak.termelLista;
            List<Szallitmany> tesztszall = Listak.szallitLista;


            //Deszka adatok lekérése a hibák létrehozásához
            deszkaA = tesztkak.ToArray();
            //Fafaj adatok lekérése a hibák létrehozásához
            faA = tesztfa.ToArray();
            //Termelés adatok lekérése a hibák létrehozásához
            termelA = tesztterm.ToArray();
            //Szállítmány adatok lekérése a hibák létrehozásához
            szallA = tesztszall.ToArray();

            deszkaB = tesztkak.ToArray();
            faB = tesztfa.ToArray();
            termelB = tesztterm.ToArray();
            szallB = tesztszall.ToArray();
            ment_gomb.Enabled = false;

        }

        private void TroubleMegjelenítő_Load(object sender, EventArgs e)
        {

        }



        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //objektumok üres állapotba állítása
            NotroubleGrid.Rows.Clear();
            Typebx.Items.Clear();

            //eredeti deszka adatok kirajzolása
            if (TroubleList.SelectedIndex == 0)
            {
                //táblázat inicializálása
                NotroubleGrid.ColumnCount = 4;
                NotroubleGrid.Columns[0].Name = "ID";
                NotroubleGrid.Columns[1].Name = "Szinkod";
                NotroubleGrid.Columns[2].Name = "Itelet";
                NotroubleGrid.Columns[3].Name = "SzallitmanyID";

                //adatok táblázatba illesztése
                foreach (Deszka desz in deszkaA)
                {
                    NotroubleGrid.Rows.Add(new string[] { desz.GetId().ToString(), desz.GetSzinkod(), desz.GetItelet(), desz.GetSzallitmanyId().ToString() });
                }


                //táblázat inicializálása
                TroubleGrid.Rows.Clear();
                TroubleGrid.ColumnCount = 4;
                TroubleGrid.Columns[0].Name = "ID";
                TroubleGrid.Columns[1].Name = "Szinkod";
                TroubleGrid.Columns[2].Name = "Itelet";
                TroubleGrid.Columns[3].Name = "SzallitmanyID";

                //hibatípus listbox értékekkel feltöltése
                Typebx.Items.Add("Színkódhiba");
                Typebx.Items.Add("Ítélethiba");

            }

            //eredeti fafaj adatok kirajzolása
            else if (TroubleList.SelectedIndex == 1)
            {
                //táblázat inicializálása
                NotroubleGrid.ColumnCount = 4;
                NotroubleGrid.Columns[0].Name = "ID";
                NotroubleGrid.Columns[1].Name = "Vilagos_limit";
                NotroubleGrid.Columns[2].Name = "Sotet_limit";
                NotroubleGrid.Columns[3].Name = "Megnevezes";
                //adatok táblázatba illesztése
                foreach (Fafaj faf in faA)
                {
                    NotroubleGrid.Rows.Add(new string[] { faf.GetId().ToString(), faf.GetVilagos_limit().ToString(), faf.GetSotet_limit().ToString(), faf.GetMegnevezes() });
                }
                //táblázat inicializálása
                TroubleGrid.Rows.Clear();
                TroubleGrid.ColumnCount = 4;
                TroubleGrid.Columns[0].Name = "ID";
                TroubleGrid.Columns[1].Name = "Vilagos_limit";
                TroubleGrid.Columns[2].Name = "Sotet_limit";
                TroubleGrid.Columns[3].Name = "Megnevezes";

                //hibatípus listbox értékekkel feltöltése
                Typebx.Items.Add("Sötét limit hiba");
                Typebx.Items.Add("Világos limit hiba");
                Typebx.Items.Add("Ying-Yang");

            }
            //eredeti szállítmány adatok kirajzolása
            else if (TroubleList.SelectedIndex == 2)
            {
                //táblázat inicializálása
                NotroubleGrid.ColumnCount = 5;
                NotroubleGrid.Columns[0].Name = "ID";
                NotroubleGrid.Columns[1].Name = "Fafaj ID";
                NotroubleGrid.Columns[2].Name = "dátum";
                NotroubleGrid.Columns[3].Name = "Időpont";
                NotroubleGrid.Columns[4].Name = "Beszállító ID";
                //adatok táblázatba illesztése
                foreach (Szallitmany szal in szallA)
                {
                    NotroubleGrid.Rows.Add(new string[] { szal.GetId().ToString(), szal.GetFafajId().ToString(), szal.GetDatum().ToString(), szal.GetIdopont().ToString(), szal.GetBeSzallito_azonosito().ToString() });
                }
                //táblázat inicializálása
                TroubleGrid.Rows.Clear();
                TroubleGrid.ColumnCount = 5;
                TroubleGrid.Columns[0].Name = "ID";
                TroubleGrid.Columns[1].Name = "Fafaj ID";
                TroubleGrid.Columns[2].Name = "dátum";
                TroubleGrid.Columns[3].Name = "Időpont";
                TroubleGrid.Columns[4].Name = "Beszállító ID";

                //hibatípus listbox értékekkel feltöltése
                Typebx.Items.Add("Fafaj ID hiba");
                Typebx.Items.Add("Beérkezési idő hiba");
                Typebx.Items.Add("Dátumhiba");
            }

            //eredeti termelés adatok kirajzolása
            else if (TroubleList.SelectedIndex == 3)
            {
                //táblázat inicializálása
                NotroubleGrid.ColumnCount = 4;
                NotroubleGrid.Columns[0].Name = "ID";
                NotroubleGrid.Columns[1].Name = "Deszka ID";
                NotroubleGrid.Columns[2].Name = "Gép ID";
                NotroubleGrid.Columns[3].Name = "Dátum";
                //adatok táblázatba illesztése
                foreach (Termeles ter in termelA)
                {
                    NotroubleGrid.Rows.Add(new string[] { ter.GetId().ToString(), ter.GetDeszkaId().ToString(), ter.GetGepId().ToString(), ter.GetDatum().ToString() });
                }
                //táblázat inicializálása
                TroubleGrid.Rows.Clear();
                TroubleGrid.ColumnCount = 4;
                TroubleGrid.Columns[0].Name = "ID";
                TroubleGrid.Columns[1].Name = "Deszka ID";
                TroubleGrid.Columns[2].Name = "Gép ID";
                TroubleGrid.Columns[3].Name = "Dátum";

                //hibatípus listbox értékekkel feltöltése
                Typebx.Items.Add("DeszkaID hiba");
                Typebx.Items.Add("Gép ID hiba");
                Typebx.Items.Add("Dátum hiba");
            }
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            TroubleGrid.Rows.Clear();

            //értékek inicializálása

            for (int i = 0; i < faA.Length; i++)
            {
                faB[i] = new Fafaj(faA[i]);
            }
            for (int i = 0; i < deszkaA.Length; i++)
            {
                deszkaB[i] = new Deszka(deszkaA[i]);
            }
            for (int i = 0; i < szallA.Length; i++)
            {
                szallB[i] = new Szallitmany(szallA[i]);
            }
            for (int i = 0; i < termelA.Length; i++)
            {
                termelB[i] = new Termeles(termelA[i]);
            }


            /*deszkaB = (Deszka[])deszkaA.Clone();
            
            szallB=(Szallitmany[])szallA.Clone();
            termelB = (Termeles[])termelA.Clone();*/
            double perc;


            //textbox egyszerű hibakezelés
            try
            {
                perc = (Convert.ToDouble(HibaPercent.Text));
            }
            catch (Exception ex)
            {
                perc = 10;
                HibaPercent.Text = "10";
            }



            //hibatípusok szerinti hibakészítés
            if (TroubleList.SelectedIndex == 0)
            {
                if (Typebx.SelectedIndex == 0)
                {
                    deszkaB = TroubleMaker.DeszkaErrorMaker(deszkaB, perc, 1);
                }
                else
                {
                    deszkaB = TroubleMaker.DeszkaErrorMaker(deszkaB, perc, 2);
                }
                foreach (Deszka desz in deszkaB)
                {
                    TroubleGrid.Rows.Add(new string[] { desz.GetId().ToString(), desz.GetSzinkod(), desz.GetItelet(), desz.GetSzallitmanyId().ToString() });
                }
            }
            else if (TroubleList.SelectedIndex == 1)
            {
                if (Typebx.SelectedIndex == 0)
                {
                    faB = TroubleMaker.FafajErrorMaker(faB, perc, 1);
                }
                else if (Typebx.SelectedIndex == 1)
                {
                    faB = TroubleMaker.FafajErrorMaker(faB, perc, 2);
                }
                else
                {
                    faB = TroubleMaker.FafajErrorMaker(faB, perc, 3);
                }
                foreach (Fafaj faf in faB)
                {
                    TroubleGrid.Rows.Add(new string[] { faf.GetId().ToString(), faf.GetVilagos_limit().ToString(), faf.GetSotet_limit().ToString(), faf.GetMegnevezes() });
                }
            }
            else if (TroubleList.SelectedIndex == 2)
            {
                if (Typebx.SelectedIndex == 0)
                {
                    szallB = TroubleMaker.SzallitmanyErrorMaker(szallB, perc, 1);
                }
                else if (Typebx.SelectedIndex == 1)
                {
                    szallB = TroubleMaker.SzallitmanyErrorMaker(szallB, perc, 2);
                }
                else
                {
                    szallB = TroubleMaker.SzallitmanyErrorMaker(szallB, perc, 3);
                }
                foreach (Szallitmany szal in szallB)
                {
                    TroubleGrid.Rows.Add(new string[] { szal.GetId().ToString(), szal.GetFafajId().ToString(), szal.GetDatum().ToString(), szal.GetIdopont().ToString(), szal.GetBeSzallito_azonosito().ToString() });
                }
            }
            else if (TroubleList.SelectedIndex == 3)
            {
                if (Typebx.SelectedIndex == 0)
                {
                    termelB = TroubleMaker.TermelesErrorMaker(termelB, perc, 1);
                }
                else if (Typebx.SelectedIndex == 1)
                {
                    termelB = TroubleMaker.TermelesErrorMaker(termelB, perc, 2);
                }
                else
                {
                    termelB = TroubleMaker.TermelesErrorMaker(termelB, perc, 3);
                }
                foreach (Termeles ter in termelB)
                {
                    TroubleGrid.Rows.Add(new string[] { ter.GetId().ToString(), ter.GetDeszkaId().ToString(), ter.GetGepId().ToString(), ter.GetDatum().ToString() });
                }
            }
        }



        private void HibaPercent_TextChanged(object sender, EventArgs e)
        {

        }

        private void ment_gomb_Click(object sender, EventArgs e)
        {
            //Update db-be

            if (MessageBox.Show("A változások érintik az adatbázis. Folytatja a műveletet ?",
                "Mentés", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                AllUpdate();
            }
        }

        public void AllUpdate()
        {
            int db = 0;
            //Fafaj
            if (TroubleList.SelectedIndex == 1)
            {
                foreach (Fafaj fa in faA)
                {
                    foreach (Fafaj upfa in faB)
                    {

                        if (fa.GetId() == upfa.GetId())
                        {
                            if (fa.GetSotet_limit() != upfa.GetSotet_limit() || fa.GetVilagos_limit() != upfa.GetVilagos_limit())
                            {
                                fa.Update(upfa);
                                db++;
                            }
                            break;
                        }
                    }
                }
            }
            //Deszka
            if (TroubleList.SelectedIndex == 0)
            {
                foreach (Deszka d in deszkaA)
                {
                    foreach (Deszka upd in deszkaB)
                    {
                        if (d.GetId() == upd.GetId())
                        {
                            if (d.GetSzinkod() != upd.GetSzinkod() || d.GetItelet().Equals(upd.GetItelet()))
                            {
                                d.Update(upd);
                                db++;
                            }
                            break;
                        }
                    }
                }
            }
            //Termelés
            if (TroubleList.SelectedIndex == 3)
            {
                foreach (Termeles t in termelA)
                {
                    foreach (Termeles upt in termelB)
                    {
                        if (t.GetId() == upt.GetId())
                        {
                            if (t.GetGepId() != upt.GetGepId() || t.GetDeszkaId() != upt.GetDeszkaId() || t.GetDatum() != upt.GetDatum())
                            {
                                t.Update(upt);
                                db++;
                            }
                            break;
                        }
                    }
                }
            }
            //Szállítmány
            if (TroubleList.SelectedIndex == 2)
            {
                foreach (Szallitmany sz in szallA)
                {
                    foreach (Szallitmany upsz in szallB)
                    {
                        if (sz.GetId() == upsz.GetId())
                        {
                            if (sz.GetFafajId() != upsz.GetFafajId() || sz.GetBeSzallito_azonosito() != upsz.GetBeSzallito_azonosito()
                                || sz.GetDatum() != upsz.GetDatum() || sz.GetIdopont() != upsz.GetIdopont())
                            {
                                sz.Update(upsz);
                                db++;
                            }
                            break;
                        }
                    }
                }
            }
            MessageBox.Show("Sikeres Adatbázis módosítás ! \n " + db + "  rekord frissült");
        }

        private void login_gomb_Click(object sender, EventArgs e)
        {
            if (Bejelentkezes())
            {
                pswBox.Text = "";
                ment_gomb.Enabled = true;
                foreach (Felhasznalo felh in Listak.felhLista)
                {

                    logg.Text = "Üdv " + felh.GetNev() + " !";
                }

            }
        }

        public bool Bejelentkezes()
        {

            foreach (Felhasznalo felh in Listak.felhLista)
            {
                if (pswBox.Text == felh.GetJelszo())
                {
                    MessageBox.Show("Sikeres Bejelntkezés");

                    return true;
                }


            }
            MessageBox.Show("Hibás jelszó");
            return false;
        }
    }
}
