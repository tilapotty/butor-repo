﻿namespace TestSample
{
    partial class PassChangerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxRegi = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxUj1 = new System.Windows.Forms.TextBox();
            this.textBoxUj2 = new System.Windows.Forms.TextBox();
            this.buttonMent = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Régi jelszó:";
            // 
            // textBoxRegi
            // 
            this.textBoxRegi.Location = new System.Drawing.Point(9, 44);
            this.textBoxRegi.Name = "textBoxRegi";
            this.textBoxRegi.Size = new System.Drawing.Size(158, 20);
            this.textBoxRegi.TabIndex = 1;
            this.textBoxRegi.UseSystemPasswordChar = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Új jelszó:";
            // 
            // textBoxUj1
            // 
            this.textBoxUj1.Location = new System.Drawing.Point(9, 102);
            this.textBoxUj1.Name = "textBoxUj1";
            this.textBoxUj1.Size = new System.Drawing.Size(158, 20);
            this.textBoxUj1.TabIndex = 3;
            this.textBoxUj1.UseSystemPasswordChar = true;
            // 
            // textBoxUj2
            // 
            this.textBoxUj2.Location = new System.Drawing.Point(9, 128);
            this.textBoxUj2.Name = "textBoxUj2";
            this.textBoxUj2.Size = new System.Drawing.Size(158, 20);
            this.textBoxUj2.TabIndex = 5;
            this.textBoxUj2.UseSystemPasswordChar = true;
            // 
            // buttonMent
            // 
            this.buttonMent.Location = new System.Drawing.Point(60, 154);
            this.buttonMent.Name = "buttonMent";
            this.buttonMent.Size = new System.Drawing.Size(107, 31);
            this.buttonMent.TabIndex = 6;
            this.buttonMent.Text = "Mentés";
            this.buttonMent.UseVisualStyleBackColor = true;
            this.buttonMent.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxUj2);
            this.groupBox1.Controls.Add(this.buttonMent);
            this.groupBox1.Controls.Add(this.textBoxUj1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxRegi);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(192, 202);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Jelszócsere";
            // 
            // PassChangerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(217, 230);
            this.Controls.Add(this.groupBox1);
            this.Name = "PassChangerForm";
            this.Text = "Jelszócsere";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxRegi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxUj1;
        private System.Windows.Forms.TextBox textBoxUj2;
        private System.Windows.Forms.Button buttonMent;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}