﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;

namespace TestSample
{
    class SeasonalityAnalysis
    {
        public static DataTable tableWithSeasonality(DataTable table)
        {
            // Lineáris trend számítása
            LinearTrend linearTrend = new LinearTrend();
            var values = (from row in table.AsEnumerable()
                          select Convert.ToDouble(row["value"]))
                         .ToArray();
            linearTrend.setBetas(values);

            // Trend hozzáillesztése a táblához
            table.Columns.Add("trend");
            for (int t = 0; t < table.Rows.Count; t++)
                table.Rows[t][table.Columns.Count - 1] = linearTrend.getTrend(t + 1);

            // Szezonok kicsomagolása
            int numberOfPeriods = table.DefaultView.ToTable(true, "season").Rows.Count;

            // Szezonális eltérések
            double[] seasonalDifference = new double[numberOfPeriods];
            for (int p = 0; p < numberOfPeriods; p++)
            {
                var realValues = (from row in table.AsEnumerable()
                                  where Convert.ToInt16(row["season"]) == (p + 1)
                                  select Convert.ToDouble(row["value"])).ToArray();

                var trendValues = (from row in table.AsEnumerable()
                                   where Convert.ToInt16(row["season"]) == (p + 1)
                                   select Convert.ToDouble(row["trend"])).ToArray();

                double tmp = 0;
                for (int i = 0; i < realValues.Length; i++)
                    tmp += (realValues[i] - trendValues[i]);

                seasonalDifference[p] = tmp / numberOfPeriods;
            }

            // Szezonális eltérések átlaga
            double seasonMean = seasonalDifference.Sum() / numberOfPeriods;

            // Korrigált szezonális eltérések
            double[] corrigatedSeasonalDifferences = new double[numberOfPeriods];
            for (int p = 0; p < numberOfPeriods; p++)
                corrigatedSeasonalDifferences[p] = (seasonalDifference[p] - seasonMean);

            // Szezonális eltéréssel kiegészített trend hozzáillesztése táblához
            table.Columns.Add("trendWithSeasonality");
            for (int t = 0; t < table.Rows.Count; t++)
            {
                double csD = corrigatedSeasonalDifferences[Convert.ToInt16(table.Rows[t]["season"]) - 1];
                table.Rows[t][table.Columns.Count - 1] = Convert.ToDouble(table.Rows[t]["trend"]) + csD;
            }

            return table;
        }
    }
}
