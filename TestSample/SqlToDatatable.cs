﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestSample;

namespace TestSample
{
    class SqlToDatatable
    {
        public static DataTable readSQL(String query)
        {
            // Forrásadat betöltése
            SQLiteDatabase database = new SQLiteDatabase("../../BalintDB.s3db");
            DataTable sourceTable = database.GetDataTable(query);

            // Új üres tábla
            DataTable resultTable = new DataTable();
            resultTable.Columns.Add("year");
            resultTable.Columns.Add("season");
            resultTable.Columns.Add("value");

            // Szezonok kigyűjtése
            List<String> seasonList = new List<String>();
            foreach (DataRow row in sourceTable.Rows)
            {
                String[] chunk = row[0].ToString().Split('.');
                seasonList.Add(chunk[1] + "." + chunk[2]);
            }
            seasonList = seasonList.Distinct().ToList();
           
            // Adatok előkészítése
            foreach (DataRow row in sourceTable.Rows)
            {
                String[] chunk = row[0].ToString().Split('.');
                String season = chunk[1] + "." + chunk[2];

                DataRow newRow = resultTable.NewRow();
                newRow["year"] = chunk[0];
                newRow["season"] = seasonList.IndexOf(season) + 1;
                newRow["value"] = row[1];

                resultTable.Rows.Add(newRow);
            }

            return resultTable;
        }

        
    }
}
