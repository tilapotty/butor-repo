﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading;
using System.Data.SQLite;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace TestSample{

    class Randomclass
    {
        public int d_szinkod;
        public String d_itelet;

        public List<int> sql_fafaj_id=new List<int>();
        public int fafaj_id;
        public DateTime datum;
        public int ido = 0; 
        public int besz_azonosito;

        public List<string> fafaj_nev=new List<string>();
        public List<int> vilagos_limit=new List<int>();
        public List<int> sotet_limit=new List<int>();

        public int sql_szallitmany_id;
        public DateTime sql_datum;
        public int sql_ido;

        public int sql_deszka_id;
        public int gep_id;
        public int muszak;
        public int sql_muszak;

        public DateTime sql_d;
        public String d;
        public SQLiteConnection sqlite;

        #region getter, setter

        public DateTime Sql_d
        {
            get;
            set;
        }

        public List<int> Sql_fafaj_id
        {
            get; set;
        }

        public List<string> Fafaj_nev
        {
            get;
            set;
        }

        public List<int> Vilagos_limit
        {
            get;
            set;
        }

        public List<int> Sotet_limit
        {
            get;
            set;
        }

        public int Sql_szallitmany_id
        {
            get;
            set;
        }

        public DateTime Sql_datum
        {
            get;
            set;
        }

        public int Sql_deszka_id
        {
            get;
            set;
        }

        public int Sql_muszak
        {
            get;
            set;
        }

        public int D_szinkod
        {
            get { return d_szinkod; }
            set { d_szinkod = value; }
        }

        public String D_itelet
        {
            get { return d_itelet; }
            set { d_itelet = value; }
        }

        public int Fafaj_id
        {
            get { return fafaj_id; }
            set { fafaj_id = value; }
        }

        public DateTime Datum
        {
            get { return datum; }
            set { datum = value; }
        }

        public int Ido
        {
            get { return ido; }
            set { ido = value; }
        }

        public int Besz_azonosito
        {
            get { return besz_azonosito; }
            set { besz_azonosito = value; }
        }

        public int Gep_id
        {
            get { return gep_id; }
            set { gep_id = value; }
        }

        public int Muszak
        {
            get { return muszak; }
            set { muszak = value; }
        }
        #endregion

        public Randomclass()
        {
            datum = new DateTime(2015, 1, 1);
            d= datum.ToString("yyyy-MM-dd");
            //MessageBox.Show(d);
            //MessageBox.Show(d.GetType().ToString());
            datum = DateTime.Parse(d);

            sql_datum = new DateTime();
            sql_d = sql_datum;
            
            
            //MessageBox.Show(datum.ToString("yyyy-MM-dd"));
            //MessageBox.Show(datum.GetType().ToString());
            sqlite = new SQLiteConnection("Data Source=../../TilaEroforrasok/SZTA2Butor.s3db;Version=3;");
            //Console.ForegroundColor = ConsoleColor.Red;
            Random rand = new Random((int)DateTime.Now.Ticks);
            int numIterations = 0;
            int numIterations2 = 0;
            numIterations = rand.Next(1, 4);
            numIterations2 = rand.Next(1, 6);
            Console.WriteLine("{0}" + " fatípus" + "{1}" + " beszállító", numIterations, numIterations2);
            Console.Write("\r{0}\n", DateTime.Now); //kijelzi az időt //\r kapcsoló fontos gyakorlatilag önmagára ír ki
            
            DB_Fafaj();
            //Szin_Itelet(d_szinkod,fafaj_id);
            DB_Szallitmany();
            DB_Deszka();

            this.Fafaj_id = numIterations;
            this.Besz_azonosito = numIterations2;
            DB_Szallitmany_Insert();
            Datum_no(datum);
            //MessageBox.Show("Random példányosítás");
            this.Ido++;

            System.Timers.Timer myTimer2 = new System.Timers.Timer();
            myTimer2.Elapsed += new ElapsedEventHandler(DisplayTimeEvent);
            myTimer2.Interval = 300000; //1000 ms-ra állítjuk be az //időzítő esemény meghívását
            myTimer2.Start(); //timer indítás

            System.Timers.Timer myTimer = new System.Timers.Timer();
            myTimer.Elapsed += new ElapsedEventHandler(DisplayTimeEvent2);
            myTimer.Interval = 500; //1000 ms-ra állítjuk be az //időzítő esemény meghívását
            myTimer.Start(); //timer indítása

            
            

            //DB_Fafaj();
            ////Szin_Itelet(d_szinkod,fafaj_id);
            //DB_Szallitmany();
            //DB_Deszka();
            //DB_Deszka_Insert();
            //DB_Szallitmany_Insert();
            //DB_Termeles_Insert();
            
        }
        //sziamia

        public void DB_Fafaj()
        {
            //sqlite = new SQLiteConnection("Data Source=../../TilaEroforrasok/SZTA2Butor.s3db;Version=3;");
            //SQLiteDataAdapter ad;

            //DataTable dt = new DataTable();

            string query = "SELECT * FROM Fafaj;";
            //MessageBox.Show("fafaj selct");
            
            //try
            //{
            //    SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
            //    cmd = sqlite.CreateCommand();
            //    cmd.CommandText = query;//set the passed query
                
            //    ad = new SQLiteDataAdapter(cmd);
            //    ad.Fill(dt); //fill the datasource
            //    //MessageBox.Show(ad.SelectCommand.ToString());
            //}
            //catch (SQLiteException ex)
            //{
            //    //Add your exception code here.
            //}
                
            SQLiteCommand command = new SQLiteCommand(query, sqlite);
            SQLiteDataReader reader = command.ExecuteReader();

            while(reader.Read())
            {
                //SQL_szallitmany_id = reader.GetInt32(0);
                //SQL_datum =
                //    MessageBox.Show(reader.GetDateTime(2).ToString());
                //SQL_ido = reader.GetInt32(3);
                sql_fafaj_id.Add(reader.GetInt32(0));
                fafaj_nev.Add(reader.GetString(1));
                vilagos_limit.Add(reader.GetInt32(2));
                sotet_limit.Add(reader.GetInt32(3));
            }
            reader.Close();
            //MessageBox.Show("SQL_fafaj_id 1: " + sql_fafaj_id.IndexOf(1).ToString());
            //MessageBox.Show(sql_fafaj_id[2].ToString());
            //MessageBox.Show(fafaj_nev[2].ToString());
            //MessageBox.Show(vilagos_limit[2].ToString());
            //MessageBox.Show(sotet_limit[2].ToString());
            sqlite.Close();
            Szin_Itelet(d_szinkod, fafaj_id);          
            //MessageBox.Show(dt.ToString());
            //return dt;


        }

        public DataTable DB_Szallitmany_Insert()
        {
            sqlite = new SQLiteConnection("Data Source=../../TilaEroforrasok/SZTA2Butor.s3db;Version=3;");
            SQLiteDataAdapter ad;
            int f = fafaj_id;
            DateTime d = datum;
            int i = ido;
            int ba = besz_azonosito;
            DataTable dt = new DataTable();

            string query9 = "Insert into Szallitmany(FafajID, Datum, Idopont, Beszallito_azonosito)values('" + f + "','" + d.ToString("yyyy-MM-dd") + "','" + i + "','" + ba + "');";

            //MessageBox.Show("Szallitmany insert");
            
            try
            {
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                cmd.CommandText = query9;//set the passed query
                ad = new SQLiteDataAdapter(cmd);
                ad.Fill(dt); //fill the datasource
                
            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
            }
            sqlite.Close();
            //MessageBox.Show(dt.ToString());
            return dt;


        }

        public void DB_Szallitmany()
        {
            //sqlite = new SQLiteConnection("Data Source=../../../TilaEroforrasok/SZTA2Butor.s3db;Version=3;");
            
            SQLiteDataAdapter ad;

            DataTable dt = new DataTable();

            string query = "SELECT ID, FafajID, Datum, Beszallito_azonosito FROM Szallitmany;";
            //MessageBox.Show("Szallitmany select");

            //try
            //{
            //    SQLiteCommand cmd;
            sqlite.Open();  //Initiate connection to the db
            //    cmd = sqlite.CreateCommand();
            //    cmd.CommandText = query;//set the passed query
            //    ad = new SQLiteDataAdapter(cmd);
            //    ad.Fill(dt); //fill the datasource

            //}
            //catch (SQLiteException ex)
            //{

            //    //Add your exception code here.
            //}

            SQLiteCommand command = new SQLiteCommand(query, sqlite);
            SQLiteDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
            sql_szallitmany_id=reader.GetInt32(0);
            sql_d = reader.GetDateTime(2);
            //sql_ido = reader.GetInt32(3);
            }
            reader.Close();
            

            sqlite.Close();
            //MessageBox.Show(dt.ToString());
            
        }

        public DataTable DB_Deszka_Insert()
        {
            sqlite = new SQLiteConnection("Data Source=../../TilaEroforrasok/SZTA2Butor.s3db;Version=3;");
            SQLiteDataAdapter ad;
            
            DataTable dt = new DataTable();
            int s = d_szinkod;
            string i = d_itelet;
            int sa = sql_szallitmany_id;
            int idop = ido;
            //MessageBox.Show(sa.ToString() + "deszka insert");
            string query3 = "INSERT INTO Deszka (Szinkod, Itelet, SzallitmanyID)VALUES('" + s + "','" + i.ToString() + "','" + sa + "');";
            //string query1 = "INSERT INTO Fafaj (Megnevezes, Vilagos_limit, Sotet_limit) VALUES('Bükk',120,180)"; 
            //MessageBox.Show("deszka insert");
            try
            {
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                cmd.CommandText = query3;//set the passed query
                ad = new SQLiteDataAdapter(cmd);
                ad.Fill(dt); //fill the datasource
                        
            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
                //MessageBox.Show("Hiba");
            }
            catch (Exception e)
            {
                //MessageBox.Show("Exception");
            }
            sqlite.Close();
            DB_Termeles_Insert();
            return dt;

            
        }

        public void DB_Deszka()
        {
            SQLiteDataAdapter ad;

            DataTable dt = new DataTable();

            string query = "SELECT ID FROM Deszka;";

            //MessageBox.Show("deszka select");
            try
            {
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                cmd.CommandText = query;//set the passed query
                ad = new SQLiteDataAdapter(cmd);
                ad.Fill(dt); //fill the datasource

            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
            }
            
            SQLiteCommand command = new SQLiteCommand(query, sqlite);
            SQLiteDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                sql_deszka_id = reader.GetInt32(0);
            }
            reader.Close();

            sqlite.Close();
            //MessageBox.Show(dt.ToString());
            
        }

        public DataTable DB_Termeles_Insert()
        {

            sqlite = new SQLiteConnection("Data Source=../../TilaEroforrasok/SZTA2Butor.s3db;Version=3;");
            SQLiteDataAdapter ad;
            int did = sql_deszka_id;
            int gid = gep_id;
            int m = sql_muszak;
            DateTime d = sql_d;
            int i = sql_ido;
            //MessageBox.Show(sql_ido.ToString());
            DataTable dt = new DataTable();

            string query = "INSERT INTO Termeles(DeszkaID, GepID, Datum)VALUES('" + did + "','" + gid + "','" + d.ToString("yyyy-MM-dd") + "');";

            //MessageBox.Show("termeles insert");
            try
            {
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                cmd.CommandText = query;//set the passed query
                ad = new SQLiteDataAdapter(cmd);
                ad.Fill(dt); //fill the datasource

            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
            }
            catch (Exception e)
            {
                //MessageBox.Show("termeles hiba");
            }
            sqlite.Close();
            //MessageBox.Show(dt.ToString());
            return dt;


        }

        public  void DisplayTimeEvent2(object source, ElapsedEventArgs e)
        {
            //int s = d_szinkod;
            //MessageBox.Show(s.ToString());
            //int f = fafaj_id;
            //MessageBox.Show(f.ToString());
            //Console.ForegroundColor = ConsoleColor.Yellow;
            int i = ido;

            Random rand = new Random((int)DateTime.Now.Ticks);
            int numIterations = 0;
            int numIterations2 = 0;
            numIterations = rand.Next(100, 301);
            numIterations2 = rand.Next(1, 10);
            Console.WriteLine("{0}", numIterations);
            Console.Write("\r{0}\n", DateTime.Now); //kijelzi az időt //\r kapcsoló fontos gyakorlatilag önmagára ír ki

            this.D_szinkod = numIterations;
            this.Gep_id = numIterations2;
            //MessageBox.Show(fafaj_id + " fafaj");
            //MessageBox.Show(sql_fafaj_id[2].ToString());
            Szin_Itelet(d_szinkod, fafaj_id);
            DB_Deszka_Insert();
            i++;
            ido = i;
            //DB_Termeles_Insert();
        }


        public void DisplayTimeEvent(object source, ElapsedEventArgs e)
        {
            //Console.ForegroundColor = ConsoleColor.Green;
            int s = d_szinkod;
            int i = ido;
            //MessageBox.Show(d_szinkod.ToString());
            Random rand = new Random((int)DateTime.Now.Ticks);
            int numIterations = 0;
            int numIterations2 = 0;
            numIterations = rand.Next(1, 4);
            numIterations2 = rand.Next(1, 6);
            Console.WriteLine("{0}" + " fatípus"+ "{1}"+ " beszállító", numIterations, numIterations2);
            Console.Write("\r{0}\n", DateTime.Now); //kijelzi az időt //\r kapcsoló fontos gyakorlatilag önmagára ír ki

            this.Fafaj_id = numIterations;
            this.Besz_azonosito = numIterations2;
            Datum_no(datum);
            i++;
            ido = i;
            datum = Datum_no(datum);
            //MessageBox.Show(datum.ToString("yy-M-d"));
            DB_Szallitmany_Insert();
            //MessageBox.Show(besz_azonosito.ToString());
        }

        public DateTime Datum_no(DateTime datum)
        {
            int intY = datum.Year;
            int intM = datum.Month;
            int intD = datum.Day;

            DateTime newdatum = new DateTime(intY,intM,(intD + 1));
            DateTime olddatum = newdatum;
            //MessageBox.Show(datum.ToString("yyyy-MM-dd"));
            //MessageBox.Show(newdatum.ToString("yyyy-MM-dd"));
            //MessageBox.Show(olddatum.ToString("yy-MM-dd"));
            return olddatum;
            

        }

        public void Szin_Itelet(int szinkod, int fafaj_id)
        {
            //MessageBox.Show(sql_fafaj_id[2].ToString());
            //MessageBox.Show(fafaj_nev[2].ToString());
            //MessageBox.Show(vilagos_limit[2].ToString());
            //MessageBox.Show(sotet_limit[2].ToString());

            if (fafaj_id == sql_fafaj_id[0])
            {
                if (d_szinkod < vilagos_limit[0])
                {
                    d_itelet = "Világos";
                }
                else if (d_szinkod > sotet_limit[0])
                {
                    d_itelet = "Sötét";
                }
                else
                {
                    d_itelet = "Általános";
                }
            }
            else if (fafaj_id == sql_fafaj_id[1])
            {
                if (d_szinkod < vilagos_limit[1])
                {
                    d_itelet = "Világos";
                }
                else if (d_szinkod > sotet_limit[1])
                {
                    d_itelet = "Sötét";
                }
                else
                {
                    d_itelet = "Általános";
                }
            }
            else if (fafaj_id == sql_fafaj_id[2])
            {
                if (d_szinkod < vilagos_limit[2])
                {
                    d_itelet = "Világos";
                }
                else if (d_szinkod > sotet_limit[2])
                {
                    d_itelet = "Sötét";
                }
                else
                {
                    d_itelet = "Általános";
                }
            }
            //MessageBox.Show(d_itelet); 
        }

        public void Muszak_valaszt()
        {
            if (this.Ido < 6 && this.Ido > 22)
            {
                this.Muszak = 3;
            }
            else if (this.Ido < 14 && this.Ido >= 6)
            {
                this.Muszak = 1;
            }
            else if (this.Ido < 22 && this.Ido >= 14)
            {
                this.Muszak = 2;
            }
        }
    }

}
