﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSample
{
    class GaussianElimination
    {
        public static double[,] SolveEquationSystem(double[,] matrix)
        {
            int rowNumber = matrix.GetUpperBound(0) + 1;
            int colNumber = matrix.GetUpperBound(1) + 1;

            if (matrix == null || matrix.Length != rowNumber * (rowNumber + 1))
                throw new ArgumentException("Bemeneti mátrixnak n*n+1-esnek kell lennie.");
            if (rowNumber < 1)
                throw new ArgumentException("Mátrixnak legfeljebb egy sornak kell rendelkeznie.");

            // Pivotálás
            for (int col = 0; col + 1 < rowNumber; col++)
                if (matrix[col, col] == 0)
                {
                    // Nem-nulla együttható megtalálása
                    int swapRow = col + 1;
                    for (; swapRow < rowNumber; swapRow++)
                        if (matrix[swapRow, col] != 0)
                            break;

                    if (matrix[swapRow, col] != 0) // Sikerült találni egyet?
                    {
                        // Ha igen, akkor csere
                        double[] tmp = new double[rowNumber + 1];
                        for (int i = 0; i < rowNumber + 1; i++)
                        {
                            tmp[i] = matrix[swapRow, i];
                            matrix[swapRow, i] = matrix[col, i];
                            matrix[col, i] = tmp[i];
                        }
                    }
                    else
                        return null; // A mátrixnak nincs megoldása
                }

            // Elimináció
            for (int sourceRow = 0; sourceRow + 1 < rowNumber; sourceRow++)
            {
                for (int destRow = sourceRow + 1; destRow < rowNumber; destRow++)
                {
                    double df = matrix[sourceRow, sourceRow];
                    double sf = matrix[destRow, sourceRow];
                    for (int i = 0; i < rowNumber + 1; i++)
                        matrix[destRow, i] = matrix[destRow, i] * df - matrix[sourceRow, i] * sf;
                }
            }

            // Visszaillesztés
            for (int row = rowNumber - 1; row >= 0; row--)
            {
                double f = matrix[row, row];
                if (f == 0)
                    return null;

                for (int i = 0; i < rowNumber + 1; i++)
                    matrix[row, i] /= f;

                for (int destRow = 0; destRow < row; destRow++)
                {
                    matrix[destRow, rowNumber] -= matrix[destRow, row] * matrix[row, rowNumber];
                    matrix[destRow, row] = 0;
                }
            }

            return matrix;
        }
    }
}
