﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading;


namespace ConsoleApplication1
{
    class Program
    {
        static Random rnd = new Random();
        
        static void Main(string[] args)
        {
            //Console.ForegroundColor = ConsoleColor.Red;
            Random rand = new Random((int)DateTime.Now.Ticks);
            int numIterations = 0;
            numIterations = rand.Next(0, 31);
            Console.WriteLine("{0}", numIterations);
            Console.Write("\r{0}\n", DateTime.Now); //kijelzi az időt //\r kapcsoló fontos gyakorlatilag önmagára ír ki
            System.Timers.Timer myTimer = new System.Timers.Timer();
            myTimer.Elapsed += new ElapsedEventHandler(DisplayTimeEvent2);
            myTimer.Interval = 500; //1000 ms-ra állítjuk be az //időzítő esemény meghívását
            myTimer.Start(); //timer indítása

            System.Timers.Timer myTimer2 = new System.Timers.Timer();
            myTimer2.Elapsed += new ElapsedEventHandler(DisplayTimeEvent);
            myTimer2.Interval = 300000; //1000 ms-ra állítjuk be az //időzítő esemény meghívását
            myTimer2.Start(); //timer indítás
            
            Console.ReadLine();
        }

        static void DisplayTimeEvent2(object source, ElapsedEventArgs e)
        {

            //Console.ForegroundColor = ConsoleColor.Yellow;

            Random rand = new Random((int)DateTime.Now.Ticks);
            int numIterations = 0;
            numIterations = rand.Next(100, 301);
            Console.WriteLine("{0}", numIterations);
            Console.Write("\r{0}\n", DateTime.Now); //kijelzi az időt //\r kapcsoló fontos gyakorlatilag önmagára ír ki
        }
        

        public static void DisplayTimeEvent(object source, ElapsedEventArgs e)
        {
            //Console.ForegroundColor = ConsoleColor.Green;
            
            Random rand = new Random((int)DateTime.Now.Ticks);
            int numIterations = 0;
            numIterations = rand.Next(100, 201);
            Console.WriteLine("{0}", numIterations);
            Console.Write("\r{0}\n", DateTime.Now); //kijelzi az időt //\r kapcsoló fontos gyakorlatilag önmagára ír ki
        }
    }

}

